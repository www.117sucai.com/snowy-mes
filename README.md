<div align="center">
    <p align="center">
        <img src="./_web/public/logo.png" height="150" alt="logo"/>
<div><h1>数字化车间Snowy-mes</h1></div>
<div><h5>高效率、低成本、通用性强、开源、免费的MES系统</h5></div>
    </p>
</div>

### Snowy-mes

### 系统介绍   

<div><h5>数字化车间Snowy-mes是一款基于Snowy的B/S结构、开源、免费的生产执行管理系统</h5></div>
<div><h4>Snowy-mes主要目的是为中小企业提供了高效率、低成本、通用性强的一个MES系统解决方案，能够实时监控当前完成进度</h4></div>




### 系统框架
当前版本基于Snowy二次开发。详情请参照：https://gitee.com/xiaonuobase/snowy
 

### 功能简介：
<ul> 
<li>生产管理</li>
<ul>
<li> 大屏展示</li>
<div>可以从大屏展示页面看到任工序任务、在制工单数、延期工单数、工单预警等等信息。如图：</div>
<img src="./_web/picture/74a078c200a409ba431cdcff47668c5.png" height="300" alt="大屏展示"/>
<li> 基础数据</li>
<ul>
<li>工序页面:</li>
<dev>根据不同的角色来设置报工权限，为空则任何人可以进行报工</dev><br>
<img src="./_web/picture/1665971960094.jpg" height="300" alt="工序添加"/>
<li>工艺路线:</li>
<dev>将一道或多道工序进行有序排列组成一道工艺路线</dev><br>
<img src="./_web/picture/1665972452328.jpg" height="300" alt="工艺路线添加"/>
<li>产品类型:</li>
<dev>对产品进行分类</dev>
<li>产品管理:</li>
<dev>对产品进行设置</dev><br>
<img src="./_web/picture/1665973174418.jpg" height="300" alt="产品添加"/>
</ul>
<li> 生产管理</li>
<ul>
<li>生产计划</li>
<div>可从生产计划页面下发有计划开始和计划结束的多个工单，自动生成工单</div>
<img src="./_web/picture/1665973642666.jpg" height="300" alt="生产计划"/>
<li>销售订单</li>
<div>销售人员通过本页面进行工单的下发</div>
<img src="./_web/picture/1665974311566.jpg" height="300" alt="销售订单"/>
<li>工单</li>
<div>通过工单页面可添加工单，并且可以查看每个工单的状态、时间、进度、计划数、报工时长等等信息。</div>
<img src="./_web/picture/1665974369342.png" height="300" alt="工单"/>
<li>任务</li>
<div>可以进行对任务的开始和结束处理，只有工单状态为执行中的时候才能修改，并且可以查看关于自己的任务的一些基本信息。此版本为只有顺序型工单，上一个任务未开始，下一个任务不能开始</div>
<img src="./_web/picture/1665974567190.png" height="300" alt="任务"/>
<li>报工</li>
<div>生产人员进行报工，只能有报工权限的角色才能进行报工</div>
<img src="./_web/picture/1665974900719.jpg" height="300" alt="报工"/>
</ul>
<li> 采购管理</li>
<img src="./_web/picture/961ee8f41c8460848b9b8b137fd469d.png" height="300" alt="采购管理"/>
<li> 库存管理</li>
<img src="./_web/picture/79ead5d3e6637ca5ad49ace85ed00ee.png" height="300" alt="库存管理"/>
<li> 字段配置</li>
<img src="./_web/picture/8499fe9760d27469fc842e07f38fc69.png" height="300" alt="字段配置"/>
<li> 编号规则</li>
<img src="./_web/picture/31416a73323a29dabc1998cbcd98f5e.png" height="300" alt="编号规则"/>
</ul>
<li>客户关系</li>
<ul>
<li> 供应商管理</li>
<img src="./_web/picture/8b9a3e9567873ff21768fb2b94ce42a.png" height="300" alt="供应商管理"/>
<li> 客户管理</li>
<img src="./_web/picture/5a36285aebc60d328656dced7666a3f.png" height="300" alt="客户管理"/>
</ul>
</ul>

### 安装部署
详情请参照：https://gitee.com/xiaonuobase/snowy