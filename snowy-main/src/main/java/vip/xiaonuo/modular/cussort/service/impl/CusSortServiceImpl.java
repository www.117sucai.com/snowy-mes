/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.cussort.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.xiaonuo.core.consts.SymbolConstant;
import vip.xiaonuo.core.exception.ServiceException;
import vip.xiaonuo.core.factory.PageFactory;
import vip.xiaonuo.core.factory.TreeBuildFactory;
import vip.xiaonuo.core.pojo.node.AntdBaseTreeNode;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.core.util.PoiUtil;
import vip.xiaonuo.modular.cusinfor.entity.CusInfor;
import vip.xiaonuo.modular.cusinfor.service.CusInforService;
import vip.xiaonuo.modular.cussort.entity.CusSort;
import vip.xiaonuo.modular.cussort.enums.CusSortExceptionEnum;
import vip.xiaonuo.modular.cussort.mapper.CusSortMapper;
import vip.xiaonuo.modular.cussort.param.CusSortParam;
import vip.xiaonuo.modular.cussort.service.CusSortService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 客户分类service接口实现类
 *
 * @author czw
 * @date 2022-07-20 10:24:38
 */
@Service
public class CusSortServiceImpl extends ServiceImpl<CusSortMapper, CusSort> implements CusSortService {
    @Resource
    CusInforService cusInforService;

    @Override
    public PageResult<CusSort> page(CusSortParam cusSortParam) {
        QueryWrapper<CusSort> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(cusSortParam)) {

            // 根据客户分类名称 查询
            if (ObjectUtil.isNotEmpty(cusSortParam.getSortName())) {
                queryWrapper.lambda().like(CusSort::getSortName, cusSortParam.getSortName());
            }
            // 根据备注信息 查询
            if (ObjectUtil.isNotEmpty(cusSortParam.getRemark())) {
                queryWrapper.lambda().like(CusSort::getRemark, cusSortParam.getRemark());
            }
            // 根据父级 查询
            if (ObjectUtil.isNotEmpty(cusSortParam.getPid())) {
                queryWrapper.nested(item -> {
                    //查询包括自身，Pid实际为自身id
                    item.lambda().eq(CusSort::getId, cusSortParam.getPid()).
                            or().
                            like(CusSort::getPids, cusSortParam.getPid());
                });
            }
        }
        queryWrapper.orderByDesc("create_time");
        return new PageResult<>(this.page(PageFactory.defaultPage(), queryWrapper));
    }

    @Override
    public List<CusSort> list(CusSortParam cusSortParam) {
        return this.list();
    }

    @Override
    public void add(CusSortParam cusSortParam) {
        CusSort cusSort = new CusSort();
        BeanUtil.copyProperties(cusSortParam, cusSort);
        //校验参数，检查是否存在相同的名称
        checkParam(cusSortParam, false);
        this.fillPids(cusSort);
        this.save(cusSort);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<CusSortParam> cusSortParamList) {
        List<Long> cusSortIds=new ArrayList<>();
        QueryWrapper<CusInfor> queryWrapper = new QueryWrapper<>();
        List<CusInfor> cusInforsList =cusInforService.list(queryWrapper);
        cusSortParamList.forEach(cusSortParam -> {
            //调用方法，根据id查询当前客户类型数据
            CusSort cusSort = this.queryCusSort(cusSortParam);
            Long id = cusSort.getId();
            //根据节点id获取所有子节点id集合
            List<Long> childIdList = this.getChildIdListById(id);
            //如果该客户类型下存在子类型，则不可删除
            if (childIdList.size() >= 1) {
                throw new ServiceException(7, "【" + cusSort.getSortName() + "】下存在子类型，不可删除！");
            } else {
                //循环调用sql
                //QueryWrapper<CusInfor> queryWrapper = new QueryWrapper<>();
                //根据客户类型id查询客户表
                if (ObjectUtil.isNotEmpty(id)) {
                    queryWrapper.lambda().eq(CusInfor::getCusSortId, id);
                }
                //循环
                //  List<CusInfor> cusInforsList =cusInforService.list(queryWrapper);
                //如果客户类型下存在客户，则不可删除
                if (cusInforsList.size() >= 1) {
                    throw new ServiceException(8, "【" + cusSort.getSortName() + "】下存在所属客户，不可删除！");
                }
            }
            cusSortIds.add(cusSortParam.getId());
        });
        this.removeByIds(cusSortIds);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(CusSortParam cusSortParam) {
        //调用方法，根据id查询当前产品类型数据
        CusSort cusSort = this.queryCusSort(cusSortParam);
        //校验参数，检查是否存在相同的名称和编码
        checkParam(cusSortParam, true);
        BeanUtil.copyProperties(cusSortParam, cusSort);
        this.fillPids(cusSort);
        //将所有子的父id进行更新
        List<Long> childIdListById = this.getChildIdListById(cusSort.getId());
        childIdListById.forEach(subChildId -> {
            CusSort child = this.getById(subChildId);
            CusSortParam childParam = new CusSortParam();
            BeanUtil.copyProperties(child, childParam);
            this.edit(childParam);
        });
        this.updateById(cusSort);
    }

    @Override
    public CusSort detail(CusSortParam cusSortParam) {
        return this.queryCusSort(cusSortParam);
    }

    /**
     * 校验参数，检查是否存在相同的名称和编码
     *
     * @author xuyuxiang
     * @date 2020/3/25 21:23
     */
    private void checkParam(CusSortParam cusSortParam, boolean isExcludeSelf) {
        Long id = cusSortParam.getId();
        String name = cusSortParam.getSortName();
        Long pid = cusSortParam.getPid();

        //如果父id不是根节点
        if (!pid.equals(0L)) {
            CusSort cusSort = this.getById(pid);
            if (ObjectUtil.isNull(cusSort)) {
                //父机构不存在
                throw new ServiceException(CusSortExceptionEnum.ORG_NOT_EXIST);
            }
        }

        // isExcludeSelf为true代表是编辑，父id和自己的id不能一致
        if (isExcludeSelf) {
            if (cusSortParam.getId().equals(cusSortParam.getPid())) {
                throw new ServiceException(CusSortExceptionEnum.ID_CANT_EQ_PID);
            }

            // 如果是编辑，父id不能为自己的子节点
            List<Long> childIdListById = this.getChildIdListById(cusSortParam.getId());
            if (ObjectUtil.isNotEmpty(childIdListById)) {
                if (childIdListById.contains(cusSortParam.getPid())) {
                    throw new ServiceException(CusSortExceptionEnum.PID_CANT_EQ_CHILD_ID);
                }
            }
        }

        LambdaQueryWrapper<CusSort> queryWrapperByName = new LambdaQueryWrapper<>();
        queryWrapperByName.eq(CusSort::getSortName, name);

        //如果是编辑，校验名称编码时排除自身
        if (isExcludeSelf) {
            queryWrapperByName.ne(CusSort::getId, id);
        }

        int countByName = this.count(queryWrapperByName);

        if (countByName >= 1) {
            throw new ServiceException(CusSortExceptionEnum.ORG_NAME_REPEAT);
        }

    }

    /**
     * 获取客户分类
     *
     * @author czw
     * @date 2022-07-20 10:24:38
     */
    private CusSort queryCusSort(CusSortParam cusSortParam) {
        CusSort cusSort = this.getById(cusSortParam.getId());
        if (ObjectUtil.isNull(cusSort)) {
            throw new ServiceException(CusSortExceptionEnum.NOT_EXIST);
        }
        return cusSort;
    }

    @Override
    public void export(CusSortParam cusSortParam) {
        List<CusSort> list = this.list(cusSortParam);
        PoiUtil.exportExcelWithStream("SnowyCusSort.xls", CusSort.class, list);
    }


    /**
     * 获取组织机构树
     *
     * @author czw
     * @date 2020/7/21 11:55
     */
    @Override
    public List<AntdBaseTreeNode> tree(CusSortParam cusSortParam) {
        List<AntdBaseTreeNode> treeNodeList = CollectionUtil.newArrayList();
        //按AntdBaseTreeNode要求构建树形的数据结构
        this.list().forEach(CusSort -> {
            AntdBaseTreeNode proTreeNode = new AntdBaseTreeNode();
            proTreeNode.setId(CusSort.getId());
            proTreeNode.setParentId(CusSort.getPid());
            proTreeNode.setTitle(CusSort.getSortName());
            proTreeNode.setValue(String.valueOf(CusSort.getId()));
            treeNodeList.add(proTreeNode);
        });


        return new TreeBuildFactory<AntdBaseTreeNode>().doTreeBuild(treeNodeList);
    }


    /**
     * 填充父ids
     *
     * @author xuyuxiang
     * @date 2020/3/26 11:28
     */
    private void fillPids(CusSort cusSort) {
        //如果是一级菜单，则添加[0]
        if (cusSort.getPid().equals(0L)) {
            cusSort.setPids(SymbolConstant.LEFT_SQUARE_BRACKETS +
                    0 +
                    SymbolConstant.RIGHT_SQUARE_BRACKETS +
                    SymbolConstant.COMMA);
        } else {
            //获取父类型数据
            CusSort cusSort1 = this.getById(cusSort.getPid());
            //将父类型的所有父类中拼接上自身的id，即为当前添加类型的所有父类
            cusSort.setPids(cusSort1.getPids() +
                    SymbolConstant.LEFT_SQUARE_BRACKETS + cusSort1.getId() +
                    SymbolConstant.RIGHT_SQUARE_BRACKETS +
                    SymbolConstant.COMMA);
        }
    }


    /**
     * 根据节点id获取所有子节点id集合
     *
     * @author xuyuxiang
     * @date 2020/3/26 11:31
     */
    @Override
    public List<Long> getChildIdListById(Long id) {
        List<Long> childIdList = CollectionUtil.newArrayList();
        LambdaQueryWrapper<CusSort> queryWrapper = new LambdaQueryWrapper<>();
        //查询为节点id子集的数据
        queryWrapper.like(CusSort::getPids, SymbolConstant.LEFT_SQUARE_BRACKETS + id +
                SymbolConstant.RIGHT_SQUARE_BRACKETS);
        this.list(queryWrapper).forEach(cusSort -> childIdList.add(cusSort.getId()));
        return childIdList;
    }


}