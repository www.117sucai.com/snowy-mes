package vip.xiaonuo.modular.workreport.result;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import vip.xiaonuo.modular.workreport.entity.WorkReport;

import java.util.Date;

@Data
public class WorkReportResult extends WorkReport {
    @Excel(name="工作时间段")
    private String startEndTime;
    //工序姓名
    private String stepName;
    //工单编号
    private String workOrderNo;




    /**
     * 不良品数
     */
    private Integer taskBadNum;
    /**
     * 排序号
     */
    private Integer sortNum;

    /**
     * 实际结束时间
     */

    private Date factEndTime;

    /**
     * 实际开始时间
     */

    private Date factStaTime;

    /**
     * 良品数
     */
    private Integer taskGoodNum;
    /**
     * id
     */
    private Long id;

    /**
     * 计划结束时间
     */

    private Date plaEndTime;

    /**
     * 计划数
     */
    private Integer plaNum;

    /**
     * 计划开始时间
     */

    private Date plaStartTime;
    /**
     * 产品id
     */
    private Long proId;

    /**
     * 产品类型id
     */
    private Long proTypeId;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 工单id 工单编号
     */
    private Long taskWorkOrderId;

    /**
     * 工序id
     */
    private Long workStepId;
    /**
     * 编码
     */
    private String code;
    /*
    用户姓名
     */
    private String  productionUserName;
}
