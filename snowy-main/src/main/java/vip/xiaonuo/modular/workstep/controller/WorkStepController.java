/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.workstep.controller;

import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.workstep.param.WorkStepParam;
import vip.xiaonuo.modular.workstep.service.WorkStepService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 工序控制器
 *
 * @author yxc
 * @date 2022-05-24 10:24:22
 */
@RestController
public class WorkStepController {

    @Resource
    private WorkStepService workStepService;

    /**
     * 查询工序
     *
     * @author yxc
     * @date 2022-05-24 10:24:22
     */
    @Permission
    @GetMapping("/workStep/page")
    @BusinessLog(title = "工序_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(WorkStepParam workStepParam) {
        return new SuccessResponseData(workStepService.page(workStepParam));
    }

    /**
     * 添加工序
     *
     * @author yxc
     * @date 2022-05-24 10:24:22
     */
    @Permission
    @PostMapping("/workStep/add")
    @BusinessLog(title = "工序_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(WorkStepParam.add.class) WorkStepParam workStepParam) {
            workStepService.add(workStepParam);
        return new SuccessResponseData();
    }

    /**
     * 删除工序，可批量删除
     *
     * @author yxc
     * @date 2022-05-24 10:24:22
     */
    @Permission
    @PostMapping("/workStep/delete")
    @BusinessLog(title = "工序_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(WorkStepParam.delete.class) List<WorkStepParam> workStepParamList) {
            workStepService.delete(workStepParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑工序
     *
     * @author yxc
     * @date 2022-05-24 10:24:22
     */
    @Permission
    @PostMapping("/workStep/edit")
    @BusinessLog(title = "工序_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(WorkStepParam.edit.class) WorkStepParam workStepParam) {
            workStepService.edit(workStepParam);
        return new SuccessResponseData();
    }

    /**
     * 查看工序
     *
     * @author yxc
     * @date 2022-05-24 10:24:22
     */
    @Permission
    @GetMapping("/workStep/detail")
    @BusinessLog(title = "工序_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(WorkStepParam.detail.class) WorkStepParam workStepParam) {
        return new SuccessResponseData(workStepService.detail(workStepParam));
    }

    /**
     * 工序列表
     *
     * @author yxc
     * @date 2022-05-24 10:24:22
     */
    @Permission
    @GetMapping("/workStep/list")
    @BusinessLog(title = "工序_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(WorkStepParam workStepParam) {
        return new SuccessResponseData(workStepService.list(workStepParam));
    }

    /**
     * 导出系统用户
     *
     * @author yxc
     * @date 2022-05-24 10:24:22
     */
    @Permission
    @GetMapping("/workStep/export")
    @BusinessLog(title = "工序_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(WorkStepParam workStepParam) {
        workStepService.export(workStepParam);
    }

}
